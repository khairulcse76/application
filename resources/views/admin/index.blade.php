<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
        <div class="col-md-12" style="margin-top: 100px">
        <div class="col-md-4"></div>
        <div class="col-md-4 well">
            <form>
                @section('ragistrationNameF')
                @show
                <div class="form-group">
                    <label for="email">Email:</label>
                    <input type="email" class="form-control" id="email" placeholder="Enter email">
                </div>
                <div class="form-group">
                    <label for="Rpwd">Password:</label>
                    <input type="password" class="form-control" id="Repassword" placeholder="Enter password">
                </div>
                @section('ragistrationRPass')
                @show
                <button type="submit" class="btn btn-info">Submit</button>
                <a href="/admin/registrationform" class="btn btn-info">Register</a>
            </form>
        </div>
        <div class="col-md-4"></div>
    </div>
</div>

</body>
</html>
