<!DOCTYPE html>
<html lang="en">
<head>
    <title>Dream Application</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body style="background: dimgray">
<div class="container" style="background: lightgray">
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Application.com</a>
            </div>
            <div class="collapse navbar-collapse" id="myNavbar">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="/application">Home</a></li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Application Type<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="#">Application latter</a></li>
                            <li><a href="#">Application latter Govt</a></li>
                            <li><a href="#">Application latter salary increment</a></li>
                        </ul>
                    </li>
                    <li><a href="#">About us</a></li>
                    <li><a href="/application/create">Create</a></li>
                    <li><a href="#">Our Service</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
                    <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="col-md-12">
        <div class="col-md-2">
            <div class="list-group">
                <a href="#" class="list-group-item disabled">Application</a>
                <a href="#" class="list-group-item">Application 2</a>
                <a href="#" class="list-group-item">Application 3</a>
                <a href="#" class="list-group-item">Application 4</a>
                <a href="#" class="list-group-item">Application 5</a>
                <a href="#" class="list-group-item">Application 6</a>
            </div>
        </div>

        <div class="col-md-8">
            @include('includes.success')
            @section('body')
            @show
        </div>
        <div class="col-md-2">
            @include('includes.errors')
        </div>
    </div>
    <div class="footer" style="background: black; border: 30px; margin: 0 auto;">
        <div class="col-md-12">
            <div class="col-md-4">

                <div class="col-md-4">

                </div>
            </div>
            <div class="col-md-4">© 2017 totapakhi.com . All Rights Reserved.</div>
            <div class="col-md-4">Right</div>
        </div>
    </div>
</div>
</body>
</html>