@include('includes.header')

<div class="col-md-12">
    @include('includes.sidebar')

    <div class="col-md-8">
        @include('includes.success')

        <div class="col-md-12" style="padding: 5px"><a href="/application" class="btn btn-info">Back to home</a>
            <a href="/application/create" class="btn btn-info">Make An Application</a>
        </div>

        <div class="col-sm-12">
            <div class="col-md-4">
                <label>
                    {{ $items->to }}
                </label>
            </div>
            <div class="col-md-4 pull-right">Date: {{ substr($items->created_at, 0, 11) }}</div>
            <div class="col-md-12">
                <label>
                    <strong>Sub: {{ $items->subject }}</strong>
                </label>
            </div>
            <div class="col-md-12">
                <label>
                    <p>{{ $items->description }}</p>
                    <p>{{ $items->sothat }}</p>
                    <span class="">{{ $items->sincerely }}</span>
                </label>
            </div>
        </div>
        <div class="col-md-12" style="padding: 5px"><a href="/application" class="btn btn-success">Back to home</a>
            <a href="{{ '/application/'.$items->id.'/edit' }}" class="btn btn-primary">Edit</a>
        </div>
    </div>
    <div class="col-md-2">
        Advartisement
    </div>
</div>
@include('includes.footer')
