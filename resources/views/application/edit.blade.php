@extends('application.create')

@section('editId', $item->id)
@section('editMethod')
    {{ method_field('put') }}
@endsection
@section('editTo', $item->to)
@section('editSubject', $item->subject)
@section('editDescription', $item->description)
@section('editSothat', $item->sothat)
@section('editSincerely', $item->sincerely)
@section('editbutton', 'Update/')