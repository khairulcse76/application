@extends('layouts.app')
@section('body')
    <div class="col-md-12" style="padding: 5px"><a href="application/create" class="btn btn-info">Make An
            Application</a>
        <a href="application/create" class="btn btn-info">Make An Application</a>
    </div>
    <div class="col-sm-12">
        <div class="list-group">
            <a href="" class="list-group-item active">
                <center><h4 class="list-group-item-heading">All of the application has to be made</h4></center>
            </a>
            @foreach($applications as $application)
                <a href="{{ '/application/'.$application->id }}" class="list-group-item">
                    <h4 class="list-group-item-heading">{{ $application->subject }}
                    <span class="pull-right">
                        <span class="glyphicon glyphicon-eye-open"></span>
                    </span>
                    </h4>
                </a>
            @endforeach
        </div>
    </div>
@endsection