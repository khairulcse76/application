@extends('layouts.app')

@section('body')
        <div class="col-md-12" style="padding: 5px"><a href="/application" class="btn btn-info">Back to home</a>
        </div>
        <div class="col-sm-12">
            <form class="form-horizontal" action="/application/@yield('editId')" method="post" >
                {{ csrf_field() }}
                @section('editMethod')
                    @show
                <div class="form-group">
                    <label for="to">To:</label>
                    <textarea class="form-control" rows="4" id="to" name="to" placeholder="please type whare u send this application">@yield('editTo')</textarea>
                    <label for="subject">Sub:</label>
                    <input type="text" class="form-control" id="subject" name="subject" value="@yield('editSubject')" placeholder="Application Subject">
                    <label for="description">Dear sir,</label>
                    <textarea class="form-control" rows="4" id="description" name="description" placeholder="Type hare Application description">@yield('editDescription')</textarea>
                    <label for="sothat">So That</label>
                    <textarea class="form-control" rows="2" id="sothat" name="sothat" placeholder="">@yield('editSothat')</textarea>
                    <label for="Sincerely">Sincerely yours</label>
                    <textarea class="form-control" rows="5" id="Sincerely" name="sincerely" placeholder="">@yield('editSincerely')</textarea>

                    <div class="col-md-12 footer"><br>
                        <button type="reset" class="btn btn-danger">R eset</button>
                        <button type="submit" class="btn btn-success">@yield('editbutton')Save</button>
                    </div>
                </div>
            </form>
    </div>
@endsection