<?php


$factory->define(App\Application::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'to' => $faker->name,
        'subject' => $faker->jobTitle(),
        'description' => $faker->text(),
        'sothat' => $faker->text(),
        'sincerely' => $faker->text(),
        'created_at' => $faker->dateTime(),
        'updated_at' => $faker->dateTime(),
    ];
});