<?php
namespace App\Http\Controllers;

use App\Application;
use Illuminate\Http\Request;

class ApplicationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $applications=Application::all();
        return view('application.index', compact('applications'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('/application.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $application=new Application();
        $this->validate($request,[
            'to'=>'required|min:6',
            'subject'=>'required',
            'description'=>'required',
            'sincerely'=>'required',
        ]);
        $application->to=$request->to;
        $application->subject=$request->subject;
        $application->description=$request->description;
        $application->sothat=$request->sothat;
        $application->sincerely=$request->sincerely;
        if ($application->save()){
            session()->flash('massage', 'Your Application Successfully Composed');
        }

        return redirect('/application');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $items=Application::find($id);

        return view('application.show', compact('items'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item=Application::find($id);
        return view('application.edit', compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update=Application::find($id);
        $this->validate($request,[
            'to'=>'required|min:4',
            'subject'=>'required',
            'description'=>'required',
            'sincerely'=>'required',
        ]);
        $update->to=$request->to;
        $update->subject=$request->subject;
        $update->description=$request->description;
        $update->sothat=$request->sothat;
        $update->sincerely=$request->sincerely;
        if ($update->save()){
            session()->flash('massage', 'Application Successfully Update');
        }
        return redirect('/application');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
